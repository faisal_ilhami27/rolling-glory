<?php

use App\Http\Controllers\Api\GiftController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'gift'], function () {
  Route::get('/', [GiftController::class, 'index']);
  Route::get('/{id}', [GiftController::class, 'edit']);
  Route::post('/', [GiftController::class, 'store']);
  Route::put('/{id}', [GiftController::class, 'update']);
  Route::delete('/{id}', [GiftController::class, 'destroy']);

  Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('/{id}/redeem', [GiftController::class, 'redeem']);
    Route::post('/{id}/rating', [GiftController::class, 'rating']);
  });
});
