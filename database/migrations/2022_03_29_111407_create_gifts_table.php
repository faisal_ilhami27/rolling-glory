<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('gifts', function (Blueprint $table) {
      $table->id();
      $table->string('name', 100);
      $table->integer('stock');
      $table->integer('point');
      $table->double('rating');
      $table->string('image');
      $table->text('short_desc')->nullable();
      $table->text('long_desc')->nullable();
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('gifts');
  }
};
