Teknologi yang digunakan yaitu Laravel

# cara setup di local
1. clone project
2. buat file .env copy dari .env.example
3. composer install
4. php artisan key:generate
5. php artisan migrate
6. php artisan db:seed
7. php artisan storage:link
8. php artisan serve
