<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

if (!function_exists('rules')) {
  function rules($request, $rules, $messages = [], $attributes = [])
  {
    return Validator::make($request->all(), $rules, $messages, $attributes);
  }
}

if (!function_exists('includeFilesInFolder')) {
  function includeFilesInFolder($folder)
  {
    try {
      $rdi = new RecursiveDirectoryIterator($folder);
      $it = new RecursiveIteratorIterator($rdi);

      while ($it->valid()) {
        if (!$it->isDot() && $it->isFile() && $it->isReadable() && $it->current()->getExtension() === 'php') {
          require $it->key();
        }

        $it->next();
      }
    } catch (Exception $e) {
      echo $e->getMessage();
    }
  }
}

if (!function_exists('includeRouteFiles')) {
  /**
   * @param $folder
   */
  function includeRouteFiles($folder)
  {
    includeFilesInFolder($folder);
  }
}

if (!function_exists('isActiveRoute')) {
  function isActiveRoute($route, $output = 'active')
  {
    if (Route::currentRouteName() == $route) {
      return $output;
    }
  }
}
