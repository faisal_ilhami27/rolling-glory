<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gift extends Model
{
  use HasFactory, SoftDeletes;

  protected $table = 'gifts';
  protected $fillable = ['name', 'stock', 'point', 'short_desc', 'long_desc', 'rating', 'image'];
  protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
