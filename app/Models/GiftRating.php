<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GiftRating extends Model
{
  use HasFactory;

  protected $table = 'gift_ratings';
  protected $fillable = ['user_id', 'gift_id', 'rating'];
}
