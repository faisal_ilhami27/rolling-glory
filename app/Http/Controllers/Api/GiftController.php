<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResponseFormatter;
use App\Http\Requests\GiftRequest;
use App\Http\Requests\ReddemRequest;
use App\Http\Requests\UpdateGiftRequest;
use App\Models\Gift;
use App\Models\GiftRating;
use App\Models\Redeem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GiftController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function index(Request $request)
  {
    try {
      $perPage = (empty($request->query('per_page'))) ? 10 : $request->query('per_page');
      $data = Gift::orderBy('id', 'desc')
        ->orderBy('rating', 'desc');


      return ResponseFormatter::success($data->paginate($perPage));
    } catch (\Exception $e) {
      return ResponseFormatter::error(null, $e->getMessage());
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function store(Request $request)
  {
    try {
      $validator = rules($request, GiftRequest::getRules());

      /* check validation */
      if ($validator->fails()) {
        return ResponseFormatter::error(null, $validator->errors());
      }

      $name = $request->name;
      $stock = $request->stock;
      $point = $request->point;
      $shortDesc = $request->short_desc;
      $longDesc = $request->long_desc;
      $image = $request->file('image');

      $gift = Gift::create([
        'name' => $name,
        'stock' => $stock,
        'point' => $point,
        'short_desc' => $shortDesc,
        'long_desc' => $longDesc,
        'image' => $image->store('gift', 'public'),
        'rating' => 0
      ]);

      return ResponseFormatter::success($gift, 'Data successfully added');
    } catch (\Exception $e) {
      return ResponseFormatter::error(null, $e->getMessage());
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function edit(int $id)
  {
    try {
      $data = Gift::where('id', $id)->first();

      // check data
      if (is_null($data)) {
        return ResponseFormatter::error(null, 'Data not found', 404);
      }

      return ResponseFormatter::success($data);
    } catch (\Exception $e) {
      return ResponseFormatter::error(null, $e->getMessage());
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function update(Request $request, int $id)
  {
    try {
      $validator = rules($request, UpdateGiftRequest::getRules());

      /* check validation */
      if ($validator->fails()) {
        return ResponseFormatter::error(null, $validator->errors());
      }

      $name = $request->name;
      $stock = $request->stock;
      $point = $request->point;
      $shortDesc = $request->short_desc;
      $longDesc = $request->long_desc;
      $image = $request->file('image');
      $data = Gift::where('id', $id)->first();

      // check data
      if (is_null($data)) {
        return ResponseFormatter::error(null, 'Data not found', 404);
      }

      $data->update([
        'name' => $name,
        'stock' => $stock,
        'point' => $point,
        'short_desc' => $shortDesc,
        'long_desc' => $longDesc,
        'image' => (empty($image)) ? $data->image : $image->store('gift', 'public')
      ]);

      return ResponseFormatter::success($data, 'Data successfully updated');
    } catch (\Exception $e) {
      return ResponseFormatter::error(null, $e->getMessage());
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy(int $id)
  {
    try {
      $data = Gift::where('id', $id)->first();

      // check data
      if (is_null($data)) {
        return ResponseFormatter::error(null, 'Data not found', 404);
      }

      $data->delete();
      return ResponseFormatter::success(null, 'Data successfully deleted');
    } catch (\Exception $e) {
      return ResponseFormatter::error(null, $e->getMessage());
    }
  }

  /**
   * @param Request $request
   * @param int $id
   * @return \Illuminate\Http\JsonResponse|void
   */
  public function redeem(Request $request, int $id)
  {
    try {
      $validator = rules($request, ReddemRequest::getRules());

      /* check validation */
      if ($validator->fails()) {
        return ResponseFormatter::error(null, $validator->errors());
      }

      $gift = Gift::where('id', $id)->first();

      // check data
      if (is_null($gift)) {
        return ResponseFormatter::error(null, 'Data not found', 404);
      }

      $userId = Auth::id();
      $amount = $request->amount;

      if ($gift->stock == 0) {
        return ResponseFormatter::error(null, 'Out of stock');
      }

      if ($amount > $gift->stock) {
        return ResponseFormatter::error(null, 'Product stock remaining ' . $gift->stock);
      }

      $data = Redeem::create([
        'user_id' => $userId,
        'gift_id' => $gift->id,
        'amount' => $amount
      ]);

      $gift->update([
        'stock' => $gift->stock - 1
      ]);
      return ResponseFormatter::success($data);
    } catch (\Exception $e) {
      return ResponseFormatter::error(null, $e->getMessage());
    }
  }

  /**
   * @param Request $request
   * @param int $id
   * @return \Illuminate\Http\JsonResponse|void
   */
  public function rating(Request $request, int $id)
  {
    try {
      $rating = $request->rating;
      $userId = Auth::id();
      $giftRatingByUser = GiftRating::where('user_id', $userId)->first();
      $ratings = GiftRating::where('gift_id', $id);

      // check this user whether they have given a rating or not
      if (!is_null($giftRatingByUser)) {
        return ResponseFormatter::error(null, 'Sorry, you have given rating to this product');
      }

      // check rating if greater than 5
      if ($rating > 5) {
        return ResponseFormatter::error(null, 'Rating maximum is 5');
      }

      GiftRating::create([
        'user_id' => $userId,
        'gift_id' => $id,
        'rating' => $rating
      ]);

      $amountRating = 0;
      foreach ($ratings->get() as $item) {
        $amountRating += $item->rating;
      }

      $total = intval($amountRating / $ratings->count(), 1);
      Gift::where('id', $id)->update([
        'rating' => $total
      ]);

      return ResponseFormatter::success(null, 'Successfully give rating');
    } catch (\Exception $e) {
      return ResponseFormatter::error(null, $e->getMessage());
    }
  }
}
