<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResponseFormatter;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function index()
  {
    try {
      $data = User::orderBy('id', 'desc')->get();
      return ResponseFormatter::success($data);
    } catch (\Exception $e) {
      return ResponseFormatter::error(null, $e->getMessage(), $e->getCode());
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function store(Request $request)
  {
    try {
      $validator = rules($request, UserRequest::getRules());

      /* check validation */
      if ($validator->fails()) {
        return ResponseFormatter::error(null, $validator->errors());
      }

      $name = $request->name;
      $username = $request->username;
      $password = Hash::make($request->password);
      $data = User::create([
        'name' => $name,
        'username' => $username,
        'password' => $password
      ]);

      return ResponseFormatter::success($data, 'Data successfully added');
    } catch (\Exception $e) {
      return ResponseFormatter::error(null, $e->getMessage(), $e->getCode());
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function edit(int $id)
  {
    try {
      $data = User::where('id', $id)->first();

      // check data
      if (is_null($data)) {
        return ResponseFormatter::error(null, 'Data not found', 404);
      }

      return ResponseFormatter::success($data);
    } catch (\Exception $e) {
      return ResponseFormatter::error(null, $e->getMessage(), $e->getCode());
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function update(Request $request, int $id)
  {
    try {
      $validator = rules($request, UpdateUserRequest::getRules());

      /* check validation */
      if ($validator->fails()) {
        return ResponseFormatter::error(null, $validator->errors());
      }

      $name = $request->name;
      $username = $request->username;
      $data = User::where('id', $id)->first();

      // check data
      if (is_null($data)) {
        return ResponseFormatter::error(null, 'Data not found', 404);
      }

      $data->update([
        'name' => $name,
        'username' => $username
      ]);

      return ResponseFormatter::success($data, 'Data successfully updated');
    } catch (\Exception $e) {
      return ResponseFormatter::error(null, $e->getMessage(), $e->getCode());
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy(int $id)
  {
    try {
      $data = User::where('id', $id)->first();

      // check data
      if (is_null($data)) {
        return ResponseFormatter::error(null, 'Data not found', 404);
      }

      $data->delete();
      return ResponseFormatter::success(null, 'Data successfully deleted');
    } catch (\Exception $e) {
      return ResponseFormatter::error(null, $e->getMessage(), $e->getCode());
    }
  }
}
