<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResponseFormatter;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticationController extends Controller
{
  /**
   * login user
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function login(Request $request)
  {
    try {
      $validator = rules($request, LoginRequest::getRules());
      $credentials = $request->only('username', 'password');

      /* check validation */
      if ($validator->fails()) {
        return ResponseFormatter::error(null, $validator->errors());
      }

      if (!Auth::attempt($credentials)) {
        return ResponseFormatter::error(null, 'Username or password is wrong');
      }

      $user = User::where('username', $request->username)->first();
      $token = $user->createToken($user->username)->plainTextToken;
      $data = [
        'user' => $user,
        'token' => $token
      ];
      return ResponseFormatter::success($data, 'Login successfully');
    } catch (\Exception $e) {
      return ResponseFormatter::error(null, $e->getMessage(), $e->getCode());
    }
  }

  /**
   * @return \Illuminate\Http\JsonResponse
   */
  public function logout()
  {
    auth()->user()->tokens()->delete();
    return ResponseFormatter::success(null, 'You have successfully logged out and the token was successfully deleted');
  }
}
