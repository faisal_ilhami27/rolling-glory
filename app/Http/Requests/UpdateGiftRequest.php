<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateGiftRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'name' => 'required',
      'stock' => 'required',
      'point' => 'required',
      'short_desc' => 'required',
      'long_desc' => 'required',
      'image' => 'mimes:jpg,png,jpeg'
    ];
  }

  public static function getRules()
  {
    $these = new static;
    return $these->rules();
  }
}
